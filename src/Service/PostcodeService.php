<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class PostcodeService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getDetails($postcode): array
    {
        $response = $this->client->request(
            'GET',
            'http://api.postcodes.io/postcodes/'.$postcode,
        );
        if($response->getStatusCode() === 404) {
            return ['err'=>'invalid postcode'];
        }
        return $response->toArray()['result'];
    }

}
