<?php

namespace App\Controller;

use App\Entity\Appointment;
use App\Entity\Contact;
use App\Repository\AppointmentRepository;
use App\Service\CookieService;
use App\Service\PostcodeService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class AppointmentController extends AbstractController
{
    /**
     * @Route("/api/appointment", methods={"POST"})
     */
    public function create(Request $request, AppointmentRepository $appointmentRepository, UserService $userService, SerializerInterface $serializer, CookieService $cookie): Response
    {
        $token = $cookie->getValue('jwt');
        if(!$token or !$userService->checkToken($token)) {
            return new Response(json_encode(['status'=>401, 'message'=>'token expired']), 401, ['Content-Type'=> 'application/json']);
        }
        $data = json_decode($request->getContent(), true);
        $user = $userService->getUser($token);
        $appointment = $serializer->denormalize($data, 'App\Entity\Appointment');
        $appointment->setWorkplacePostcode('cm27pj');

        // TODO: these values will be came from google api
        $appointment->setDistance(0);
        $appointment->setDepartureDate(new \DateTime());
        $appointment->setArrivalDate(new \DateTime());

        $appointment->setCreatedAt(new \DateTimeImmutable());
        $appointment->setWorker($user);
    
        $contact = $serializer->denormalize($data['contact'], 'App\Entity\Contact');

        $appointment->setClient($contact);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($contact);
        $entityManager->persist($appointment);
        $entityManager->flush();

        return $this->json([
            'status'=>200,
            'message'=>'success'
        ]);

    }

    /**
     * @Route("/api/appointment/{id}", methods={"DELETE"})
     */
    public function delete(AppointmentRepository $ar, UserService $userService, CookieService $cookie, $id): Response
    {
        $token = $cookie->getValue('jwt');
        if(!$token or !$userService->checkToken($token)) {
            return new Response(json_encode(['status'=>401, 'message'=>'token expired']), 401, ['Content-Type'=> 'application/json']);
        }
        $appointment = $ar->find($id);
        if(!$appointment) {
            return new Response(null, 404);
        }
        $contact = $appointment->getClient();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($appointment);
        $entityManager->remove($contact);
        $entityManager->flush();
        return $this->json([
            'status'=>200,
            'message'=>'success'
        ]);
    }
    /**
     * @Route("/api/appointment", methods={"PUT"})
     */
    public function update(Request $request, AppointmentRepository $ar, CookieService $cookie, UserService $userService, SerializerInterface $serializer): Response
    {
        $token = $cookie->getValue('jwt');
        if(!$token or !$userService->checkToken($token)) {
            return new Response(json_encode(['status'=>401, 'message'=>'token expired']), 401, ['Content-Type'=> 'application/json']);
        }
        $data = json_decode($request->getContent(), true);
        $appointment = $ar->find($data['id']);
        if(!$appointment) {
            return new Response(null, 404);
        }
        $serializer->denormalize($data, 'App\Entity\Appointment', 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $appointment]);
        $client = $appointment->getClient();
        $serializer->denormalize($data['contact'], 'App\Entity\Contact', 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $client]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($appointment);
        $entityManager->persist($client);
        $entityManager->flush();
        return $this->json([
            'status' => 200,
            'message' => 'success'
        ]);
    }

    /**
     * @Route("/api/appointment", methods={"GET"})
     */
    public function getAll(Request $request, AppointmentRepository $ar, SerializerInterface $serializer, CookieService $cookie, UserService $userService): Response
    {
        $data = json_decode($request->getContent(), true);
        $token = $cookie->getValue('jwt');
        if(!$token or !$userService->checkToken($token)) {
            return new Response(json_encode(['status'=>401, 'message'=>'token expired']), 401, ['Content-Type'=> 'application/json']);
        }
        if(isset($data['start']) and isset($data['stop'])) {
            $appointments = $ar->findByRestrictedCreatedAt(new \DateTime($data['start']), new \DateTime($data['stop']));
        } else {
            $appointments = $ar->findAll();
        }
        return $this->json([
            'status' => 200,
            'message' => 'success',
            'content' => [
                'appoinments' => $serializer->normalize($appointments, 'App\Entity\Appointment[]', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['password', 'appointments', '__initializer__', '__cloner__', '__isInitialized__']])
            ]
        ]);
    }

    /**
     * @Route("/api/appointment/{id}", methods={"GET"})
     */
    public function getById(AppointmentRepository $ar, UserService $userService, SerializerInterface $serializer, CookieService $cookie, PostcodeService $postcodeService, $id): Response
    {
        $token = $cookie->getValue('jwt');
        if(!$token or !$userService->checkToken($token)) {
            return new Response(json_encode(['status'=>401, 'message'=>'token expired']), 401, ['Content-Type'=> 'application/json']);
        }
        $appointment = $ar->find($id);
        if(!$appointment) {
            return new Response(null, 404);
        }
        $result = $serializer->normalize($appointment, 'App\Entity\Appointment', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['password', 'appointments', '__initializer__', '__cloner__', '__isInitialized__']]);
        $result['addressDetails'] = $postcodeService->getDetails($appointment->getTargetPostcode());
        return $this->json([
            'status' => 200,
            'message' => 'success',
            'content' => [
                'appointment' => $result
            ]
        ]);
    }

}
