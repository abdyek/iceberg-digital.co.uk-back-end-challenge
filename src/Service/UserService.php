<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\JWTService;

class UserService
{
    private $jwtService;

    public function __construct(JWTService $jwtService, UserRepository $userRepository)
    {
        $this->jwtService = $jwtService;
        $this->userRepository = $userRepository;
    }

    public function checkToken(string $token): bool
    {
        return ($this->jwtService->getPayload($token)?true:false);
    }

    public function getUser(string $token): User
    {
        $payload = $this->jwtService->getPayload($token);
        return $this->userRepository->find($payload['userId']);
    }
}
