## Clone This Repository

```sh
git clone https://gitlab.com/abdyek/iceberg-digital.co.uk-back-end-challenge ye-back-end
```

## Change Directory
```sh
cd ye-back-end
```

## Install Dependencies

```sh
composer install
```

## Check Requirements
```sh
symfony check:requirements
```

if there is any missing extension or package on your computer to built this project, this command will view you it.

## Database

Edit *.env* file to set database config and migrate it.

```sh
php bin/console doctrine:migrations:migrate
```

[Look at postman documentation](https://documenter.getpostman.com/view/11679387/UVJkBt3t)
