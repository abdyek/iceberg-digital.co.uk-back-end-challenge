<?php

namespace App\Service;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\ExpiredException;

class JWTService
{
    private $secret;

    public function __construct($secret)
    {
        $this->secret = $secret;
    }

    public function generate($userId)
    {
        $payload = [
            'userId' => $userId,
            'iat' => time(),
            'exp' => time() + 300
        ];
        return JWT::encode($payload, $this->secret, 'HS256');
    }

    public function getPayload(string $jwt): ?array
    {
        try {
            return (array) JWT::decode($jwt, new Key($this->secret, 'HS256'));
        } catch (ExpiredException $e) {
            return null;
        }
    }

}
