<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\CookieService;
use App\Service\JWTService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/api/sign-up", methods={"POST"})
     */
    public function createUser(Request $request, UserRepository $userRepository): Response
    {
        $data = json_decode($request->getContent(), true);
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPhoneNumber($data['phoneNumber']);
        $user->setFirstName($data['firstName']);
        $user->setLastName($data['lastName']);
        $user->setPassword(password_hash($data['password'], PASSWORD_DEFAULT));
        $userRepository->save($user);
        return $this->json([
            'status' => 200,
            'message' => 'success'
        ]);
    }
    /**
     * @Route("/api/sign-in", methods={"POST"})
     */
    public function generateJWT(Request $request, UserRepository $userRepository, JWTService $jwtService, CookieService $cookie): Response
    {
        $data = json_decode($request->getContent(), true);
        $user = $userRepository->findOneByEmail($data['email']);
        if(!$user or !password_verify($data['password'], $user->getPassword())) {
            return new Response(null, 401);
        }
        $token = $jwtService->generate($user->getId());
        $cookie->setCookie('jwt', $token, 300);
        return $this->json([
            'status' => 200,
            'message' => 'success',
            'content' => [
                'token' => $token
            ]
        ]);
    }
}
