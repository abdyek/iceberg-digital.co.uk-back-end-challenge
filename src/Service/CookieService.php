<?php

namespace App\Service;

class CookieService
{
    public function setCookie($key, $value, $exp): void
    {
        setCookie($key, $value, [
            'secure' => false,
            'path'=> '/',
            'expires' => time() + $exp,
            'httponly' => true,
            'samesite' => 'Lax'
        ]);
    }
    public function destroy($key): void
    {
        unset($_COOKIE[$key]);
    }

    public function getValue($key)
    {
        return (isset($_COOKIE[$key])?$_COOKIE[$key]:null);
    }
}
